import { model, Schema } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';

const ModuleSchema = new Schema({
    coursename:{type:String},
    modulename:{type:String}
    
});
ModuleSchema.plugin(mongoosePaginate);
export default model('Module', ModuleSchema);