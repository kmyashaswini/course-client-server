class Page {
    constructor(pagination) {
      this.limit = pagination.limit;
      this.page = pagination.page;
      this.totalCount = pagination.totalDocs;
    }
  }
  
  export default Page;