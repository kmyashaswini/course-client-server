import { model, Schema } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';

const CourseSchema = new Schema({
    coursename:{type:String}
    
});
CourseSchema.plugin(mongoosePaginate);
export default model('Course', CourseSchema);