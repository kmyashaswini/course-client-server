import { model, Schema } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';

const ChapterSchema = new Schema({
    modulename:{type:String},
    chaptername:{type:String},
    videoId:{type:String},
    uploadF1:{type:String},
    uploadF2:{type:String} 
});
ChapterSchema.plugin(mongoosePaginate);
export default model('Chapter', ChapterSchema);