import { courseMutations, courseQueries } from './course';
import { moduleMutations, moduleQueries } from './module';
import { chapterMutations, chapterQueries } from './chapter';
// import studentQueries  from './student/queries';

const resolvers = {
  Query: {
    ...courseQueries,
    ...moduleQueries,
    ...chapterQueries
  },
  Mutation: {
    ...courseMutations,
    ...moduleMutations,
    ...chapterMutations
  },
};

export default resolvers;