import { Module } from  '../../../db/models';

const moduleMutations = {
    createModule: async (_, {input }) => {     
      const newModule = new Module(input);
      return newModule.save();
    },
   
   
  };
  export default moduleMutations;