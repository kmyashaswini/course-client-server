import { Course } from '../../../db/models';
import Page from '../../../db/models/page';


const courseQueries={
getcourse:async (_,{ limit,page }) => {
    const coursePagination = await Course.paginate({}, { limit, page });
    console.log(coursePagination)
    return {
      course: coursePagination.docs,
      page: new Page(coursePagination),
    };
},
getallcourse:async () => {
  return await Course.find()
},
};

export default courseQueries;