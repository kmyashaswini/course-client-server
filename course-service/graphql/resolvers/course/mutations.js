import { Course } from  '../../../db/models';

const courseMutations = {
    createCourse: async (_, {input }) => {     
      const newCourse = new Course(input);
      return newCourse.save();
    },
   
   
  };
  export default courseMutations;