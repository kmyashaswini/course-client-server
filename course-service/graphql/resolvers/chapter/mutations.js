import { Chapter } from  '../../../db/models';

const chapterMutations = {
    createChapter: async (_, {input }) => {     
      const newChapter = new Chapter(input);
      return newChapter.save();
    },
   
   
  };
  export default chapterMutations;