import { port } from './config/environment';
import app from './app';
import connectDB from './db';

const start = async () => {
  try {
    await app.listen(port);
    await connectDB();
    console.log(`🚀  GraphQL server running at port: ${port}`);
  } catch (e) {
    console.log('Not able to run GraphQL server', e);
  }
};

start();
