/** @type {import('tailwindcss').Config} */
const { join } = require('path');

module.exports = {
  content: [
    "./index.html",
    join(__dirname, 'src/**/*.{js,ts,jsx,tsx}'),
  ],
  theme: {
    extend: {
       fontFamily: {
        'poppins': ['Poppins', 'sans-serif'] 
      },
      boxShadow: {
        '3xl': '0px 4px 4px rgba(0, 0, 0, 0.25)',
      }
    },
  },
  plugins: [require('tailwindcss'), require('autoprefixer')],
}
