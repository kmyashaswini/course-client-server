import React, { ReactElement } from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';
import AppLayout from './containers/AppLayout';
import LoginPage from './containers/login';
import CoursePage from './containers/Course';

type RouteDescription = {
  name: string;
  path: string;
  isPublic?: boolean;
  component: React.FC;
};

export const routes: RouteDescription[] = [
  {
    name: 'login',
    component: LoginPage,
    path: '/login',
    isPublic: true,
  },
  {
    name: 'Course',
    component: CoursePage,
    path: '/Course',
    isPublic: true,
  },
];

const AppRoutes = (): ReactElement => {
  return (
    <Routes>
      <Route path="/" element={<Navigate to="/login" />} />
      {routes.map(({ path, name, component: Component }) => (
        <Route
          key={name}
          path={path}
          element={
            <AppLayout>
              <Component />
            </AppLayout>
          }
        />
        
      ))}
      <Route path="/" element={<Navigate to="/Course" />} />
      {routes.map(({ path, name, component: Component }) => (
        <Route
          key={name}
          path={path}
          element={
            <AppLayout>
              <Component />
            </AppLayout>
          }
        />
        
      ))}
    </Routes>
  );
};

export default AppRoutes;
