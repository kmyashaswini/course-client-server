import { ReactElement } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { QueryClient, QueryClientProvider } from 'react-query';
import i18next from 'i18next';
import { I18nextProvider } from 'react-i18next';
import Toaster from './components/toaster';
import AppRoutes from './routes';

import './App.sass';

import resources from './translations';

const queryClient = new QueryClient();
i18next.init({
  interpolation: { escapeValue: false },
  lng: 'en',
  resources,
});

const App = (): ReactElement => {
  return (
    <I18nextProvider i18n={i18next}>
      <QueryClientProvider client={queryClient}>
        <Toaster />
        <BrowserRouter>
          <AppRoutes />
        </BrowserRouter>
      </QueryClientProvider>
    </I18nextProvider>
  );
};

export default App;
