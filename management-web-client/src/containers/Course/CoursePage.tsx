                import React, { ReactElement } from 'react';
import AddChapterElement from './ChapterTab/AddChapterElement';
import ChapterListingElement from './ChapterTab/ChapterListingElement';
                import AddCourseElement from './CourseTab/AddCourseElement';
                import CourseListingElement from './CourseTab/CourseListingElement';
import AddModuleElement from './ModuleTab/AddModuleElement';
import ModuleListingElement from './ModuleTab/ModuleListingElement';

                const CoursePage = (): ReactElement => {
                const [openTab, setOpenTab] = React.useState(1);

                return <div className="m-2 p-5">
                <div className="flex flex-wrap ">
                    <div className="w-full font-poppins-600">
                        <ul
                            className="flex flex-wrap -mb-px text-sm  text-center ml-6"
                            role="tablist"
                            >
                            <li className="mr-2">
                            <button
                            className={
                            "inline-block  rounded-full  border-0 px-8  font-poppins  focus:outline-0 hover:bg-[#D9D9D9] " +
                            (openTab === 1
                            ? "text-[#000000] bg-[#D9D9D9] font-bold"
                            : "text-[#00000099] bg-white font-semibold ")
                            }
                            onClick={e => {
                            e.preventDefault();
                            setOpenTab(1);
                            }}
                            data-toggle="tab"
                            role="tablist"
                            >
                            Courses
                            </button>
                            </li>
                            <li className="mr-2">
                            <button
                            className={
                            "inline-block  rounded-full  border-0 px-8  font-poppins  focus:outline-0 hover:bg-[#D9D9D9]" +
                            (openTab === 2
                            ? "text-[#000000]  bg-[#D9D9D9] font-bold "
                            : "font-semibold text-[#00000099] bg-white")
                            }
                            onClick={e => {
                            e.preventDefault();
                            setOpenTab(2);
                            }}
                            data-toggle="tab"
                            role="tablist"
                            >
                            Modules/Levels
                            </button>
                            </li>
                            <li className="mr-2">
                            <button
                            className={
                            "inline-block  rounded-full  border-0 px-8  font-poppins    focus:outline-0 hover:bg-[#D9D9D9]" +
                            (openTab === 3
                            ? "text-[#000000] bg-[#D9D9D9] font-bold "
                            : "font-semibold text-[#00000099]  bg-white")
                            }
                            onClick={e => {
                            e.preventDefault();
                            setOpenTab(3);
                            }}
                            data-toggle="tab"
                            role="tablist"
                            >
                            Chapters
                            </button>
                            </li>
                        </ul>
                        <div className="relative flex flex-col min-w-0 break-words bg-white w-full mt-6 rounded">
                            <div className="px-2 py-5 flex-auto">
                            <div className="tab-content tab-space">
                                <div className={openTab === 1 ? "block" : "hidden"} id="link1">
                                <div className="grid grid-cols-3 gap-2">
                                    <div className="">
                                    <AddCourseElement/>
                                    </div>
                                    <div className="col-span-2">
                                    <CourseListingElement/>
                                    </div>
                                </div> 
                            </div>
                            <div className={openTab === 2 ? "block" : "hidden"} id="link2">
                            <p>
                            <div className="grid grid-cols-3 gap-2">
                                    <div className="">
                                    <AddModuleElement/>
                                    </div>
                                    <div className="col-span-2">
                                    <ModuleListingElement/>
                                    </div>
                                </div> 
                            </p>
                            </div>
                            <div className={openTab === 3 ? "block" : "hidden"} >
                            <p>
                            <div className="grid grid-cols-3 gap-2">
                                    <div className="">
                                    <AddChapterElement/>
                                    </div>
                                    <div className="col-span-2">
                                    <ChapterListingElement/>
                                    </div>
                                </div> 
                            </p>
                        </div>
                    </div>
                </div>
                </div>
                </div>
                </div>
                </div>;
                };
                export default CoursePage;