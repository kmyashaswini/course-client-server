import React, { ReactElement, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBinoculars, faDeleteLeft, faEdit, faEnvelope, faEye, faTrash, faTrashCan, faArrowDownAZ, faSearch } from '@fortawesome/free-solid-svg-icons'
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import _ from "lodash";
import { GET_MODULE, Moduledata } from './graphql/query'
import { useQuery } from '@apollo/client';

const ModuleListingElement = (): ReactElement => {

  const course = useQuery<Moduledata>(
    GET_MODULE
  );
  const [data, setdata] = React.useState(course.data?.getallmodule);
  const [order, setOrder] = React.useState("ASC");
  const [posts, setposts] = React.useState(course.data?.getallmodule);
  const [serachterm, setsearchterm] = React.useState("");
  const [currentpage, setcurrentpage] = React.useState(1);
  const [isloaded, setLoaded] = React.useState(false)
  const pagesize = 4;
  const pagecount = course.data?.getallmodule ? Math.ceil(course.data?.getallmodule.length / pagesize) : 0;
  const datainitial = _(course.data?.getallmodule).slice(2).take(pagesize).value();
  const [paginatedposts, setpaginatedposts] = React.useState(datainitial);
  const pages = _.range(1, pagecount + 1)
  const pagination = (pageno: number) => {
    console.log("imin", pageno)
    setcurrentpage(pageno);
    const startindex = (pageno - 1) * pagesize;
    const paginatedPost = _(course.data?.getallmodule).slice(startindex).take(pagesize).value();
    setpaginatedposts(paginatedPost);
  }



  const nextpage = (current: number) => {
    setcurrentpage(current + 1);
    const startindex = (current) * pagesize;
    const paginatedPost = _(course.data?.getallmodule).slice(startindex).take(pagesize).value();
    setpaginatedposts(paginatedPost);
  }
  const prevpage = (current: number) => {
    setcurrentpage(current - 1);
    const startindex = (current - 2) * pagesize;
    const paginatedPost = _(course.data?.getallmodule).slice(startindex).take(pagesize).value();
    setpaginatedposts(paginatedPost);
  }
  const sorting = () => {
    console.log("sort in")
    if (order === "ASC") {
      const sorted = [...paginatedposts].sort((a, b) =>
        a.coursename.toLowerCase() > b.coursename.toLowerCase() ? 1 : -1
      );

      setpaginatedposts(sorted);
      setOrder("DSC");
    }
    if (order === "DSC") {
      const sorted = [...paginatedposts].sort((a, b) =>
        a.coursename.toLowerCase() < b.coursename.toLowerCase() ? 1 : -1
      );
      setpaginatedposts(sorted);
      setOrder("ASC");
    }

  }
  return <>
    <div onLoad={() => pagination(1)} className="flex flex-col">
      <div className="mb-3 xl:w-56">
        <input

          type="text"
          className="
        form-control
        block
        w-full
        px-3
        py-1.5
        text-sm
        font-normal
        text-gray-700
        bg-white bg-clip-padding
        border border-solid border-gray-300
        rounded
        transition
        ease-in-out
        m-0
        focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
      "
          id="exampleText0"
          value={serachterm}
          placeholder="Search" onChange={(e) => setsearchterm(e.target.value)}
        />
      </div>
      <table className="table-auto font-poppins">
        <thead >
          <tr className='text-left text-sm'>
            <th className='pb-3 pl-2'>Sl.No</th>
            <th className='pb-3 pl-2'>Module Name</th>
            <th className='pb-3' >Course Name <FontAwesomeIcon onClick={() => sorting()} className="cursor-pointer text-gray-500" icon={faArrowDownAZ} /></th>
            <th className='pb-3'>Actions</th>
          </tr>
        </thead>
        <tbody className='rounded-lg bg-[#F8F8F8] py -6 shadow-3xl font-poppins text-sm font-medium'>
          {paginatedposts.filter((val) => {
            if (serachterm == "") {
              return val;
            }
            else if (val.coursename.toLowerCase().includes(serachterm.toLowerCase())) {
              return val;
            }
          }).
            map((name, index) => {
              return (
                <tr>
                  <td className='pl-5  py-3'>{index + 1}</td>
                  <td className='pl-5  py-3'>{name.modulename}</td>
                  <td className='pl-1 py-3'>{name.coursename}</td>
                  <td><FontAwesomeIcon icon={faEye} className="inline-block pr-1" />
                    <FontAwesomeIcon icon={faEdit} className="inline-block px-1" />
                    <FontAwesomeIcon icon={faTrashCan} className="inline-block  px-1 text-rose-600" /></td>
                </tr>
              )
            })}
        </tbody>
      </table>
      <div className="flex justify-end mt-6 ">
        <nav aria-label="Page navigation example">
          <ul className="flex list-style-none">
            <li className={currentpage === 1 || currentpage <= -1 ? "page-item pointer-events-none" : "page-item"}><a
              onClick={() => prevpage(currentpage)}
              className="page-link relative block py-1.5 px-3 rounded border-0 bg-transparent hover:text-[#000] outline-none transition-all duration-300 rounded text-gray-500  focus:shadow-none"
              href="#" >Previous</a></li>
            {pages.map((page) => {
              {
                console.log(page)
                return (

                  <li className={page === currentpage ? "page-item bg-[#000] text-white text-sm rounded" : "page-item bg-gray-100 text-gray-800 text-sm rounded"}><a onClick={() => pagination(page)}

                    className="page-link relative block py-1.5 px-3 mx-0.5  border-0  outline-none text-gray-400 hover:text-white transition-all duration-300   shadow-md focus:shadow-md"
                    href="#">{page}<span className="visually-hidden"></span></a></li>

                )
              }
            })}


            <li className={currentpage >= 3 ? "page-item pointer-events-none" : "page-item"}><a onClick={() => nextpage(currentpage)}
              className="page-link relative block py-1.5 px-3 rounded border-0 bg-transparent outline-none transition-all duration-300 rounded text-gray-800 hover:text-[#000] focus:shadow-none"
              href="#">Next</a></li>
          </ul>
        </nav>
      </div>
    </div>


  </>;
};
export default ModuleListingElement;