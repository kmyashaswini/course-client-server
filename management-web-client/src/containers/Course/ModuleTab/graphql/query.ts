import { gql } from '@apollo/client';

export interface Module{
    coursename : string
    modulename : string
}

export interface Moduledata{
    getallmodule : Module[]
}
export const GET_MODULE = gql `
{
    getallmodule{
       modulename
       coursename
    }
}
`;