import { gql } from '@apollo/client';

export interface Module{
    coursename : string
    modulename:string
}
export interface newModuledetails{
    coursename:string
    modulename:string
}
export const SAVE_MODULE  = gql`
  mutation createModule($module: moduleinput!) {
    createModule(input: $module) {
      modulename
      coursename
    }
  }
`;