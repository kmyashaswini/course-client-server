import React, { ReactElement } from 'react';
import { useMutation, gql } from '@apollo/client';
import { GET_COURSE, Coursedata } from '../CourseTab/graphql/query'
import {SAVE_MODULE ,Module, newModuledetails} from './graphql/mutation'
import { useQuery } from '@apollo/client';


const AddModuleElement = (): ReactElement => {
const [coursename ,  setCourseName] = React.useState('');
const [modulename,setModuleName]=React.useState('');
const course = useQuery<Coursedata>(
    GET_COURSE
  );
  const [createModule, { error, data }] = useMutation<
    { createModule : Module },
    { module:  newModuledetails}
  >(SAVE_MODULE, {
    variables: { module : { coursename , modulename } }
  });
  function refresh(){
   window.location.reload();
 }
return <>
                        <div className="flex">
                           <div className="block p-6 rounded-lg  bg-[#D9D9D9]">
                             <select id="countries" value={coursename}  onChange={(e) => setCourseName(e.target.value)} className=" text-xs font-poppins  border-none focus:outline-none text-gray-700 rounded mb-3 block w-full p-2.5">
                                <option selected>Select Course</option>
                                {course.data && course.data.getallcourse.map((course : any,index:any) =>{
              return( <option selected>{course.coursename}</option>)
            })}
                                </select>
                             <div className="mb-4 xl:w-96">
                                 <input
                                    type="text"
                                    className="
                                    form-control
                                    block
                                    w-full
                                    px-3
                                    py-3
                                    text-xs
                                    font-normal
                                    text-gray-700
                                    bg-white bg-clip-padding
                                    border-none
                                    rounded
                                    font-poppins
                                    transition
                                    ease-in-out
                                   
                                    m-0
                                    focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
                                    "
                                    id="exampleText0"
                                    placeholder="Enter Module name"
                                    value={modulename}  onChange={(e) => setModuleName(e.target.value)}
                                    maxLength={10}
                                    minLength={2}
                                    />
                              </div>
                              <button type="button" onClick={() => {coursename && createModule()}} className=" inline-block xl:w-96 px-6 py-3 bg-black text-white font-medium text-sm w-100 leading-tight border-0  text-opacity-70 hover:shadow-sm  rounded  hover:border-0  hover:bg-gray-800   focus:outline-none  active:bg-black-800 active:shadow-lg transition duration-150 ease-in-out">Add Module/Level</button>
                           </div>
                        </div>
                   

</>;
};
export default AddModuleElement;