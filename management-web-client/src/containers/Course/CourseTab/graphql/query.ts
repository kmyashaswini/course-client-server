import { gql } from '@apollo/client';

export interface Course{
    coursename : string
}

export interface Coursedata{
    getallcourse : [Course]
}
export const GET_COURSE = gql `
{
    getallcourse{
       coursename
    }
}
`;