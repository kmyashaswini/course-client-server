import { gql } from '@apollo/client';

export interface Course{
    coursename : string
}
export interface newCoursedetails{
    coursename:string
}
export const SAVE_COURSE  = gql`
  mutation createCourse($course: courseinput!) {
    createCourse(input: $course) {
     coursename
    }
  }
`;