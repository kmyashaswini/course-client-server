import React, { ReactElement } from 'react';
import { useMutation, gql } from '@apollo/client';
import {SAVE_COURSE ,Course, newCoursedetails} from './graphql/mutation'
const AddCourseElement = (): ReactElement => {
const [openTab, setOpenTab] = React.useState(1);
const [coursename ,  setCourseName] = React.useState('');
  const [createCourse, { error, data }] = useMutation<
    { createCourse: Course },
    { course:  newCoursedetails}
  >(SAVE_COURSE, {
    variables: { course: { coursename } }
  });
  function refresh(){
   window.location.reload();
 }
return <>
                        <div className="flex">
                           <div className="block p-6 rounded-lg  bg-[#D9D9D9]">
                              <div className="mb-4 xl:w-96">
                                 <input
                                    type="text"
                                    className="
                                    form-control
                                    block
                                    w-full
                                    px-3
                                    py-3
                                    text-xs
                                    font-normal
                                    text-gray-700
                                    bg-white bg-clip-padding
                                    border-none
                                    rounded
                                    font-poppins
                                    transition
                                    ease-in-out
                                   
                                    m-0
                                    focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
                                    "
                                    id="exampleText0"
                                    placeholder="Enter course name"
                                    value={coursename}  onChange={(e) => setCourseName(e.target.value)}
                                    maxLength={10}
                                    minLength={2}
                                    />
                              </div>
                              <button type="button" onClick={() => {coursename && createCourse();refresh()}} className=" inline-block xl:w-96 px-6 py-3 bg-black text-white font-medium text-sm w-100 leading-tight border-0  text-opacity-70 hover:shadow-sm  rounded  hover:border-0  hover:bg-gray-800   focus:outline-none  active:bg-black-800 active:shadow-lg transition duration-150 ease-in-out">Add Course</button>
                           </div>
                        </div>
                   

</>;
};
export default AddCourseElement;