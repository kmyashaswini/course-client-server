import { gql } from '@apollo/client';

export interface Chapter{
    chaptername : string
    modulename : string
    videoId: string
    uploadF1 : string
    uploadF2 : string
}

export interface Chapterdata{
    getallchapter : Chapter[]
}
export const GET_CHAPTER = gql `
{
    getallchapter{
       modulename
       chaptername
       videoId
       uploadF1
       uploadF2
    }
}
`;