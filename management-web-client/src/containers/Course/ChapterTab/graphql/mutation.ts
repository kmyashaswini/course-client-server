import { gql } from '@apollo/client';

export interface Chapter{
    chaptername : string
    modulename : string
    videoId: string
    uploadF1 : string
    uploadF2 : string
}
export interface newChapterdetails{
    chaptername : string
    modulename : string
    videoId: string
    uploadF1 : string
    uploadF2 : string
}
export const SAVE_CHAPTER  = gql`
  mutation createChapter($chapter: chapterinput!) {
    createChapter(input: $chapter) {
        modulename
        chaptername
        videoId
        uploadF1
        uploadF2
    }
  }
`;