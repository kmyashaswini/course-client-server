import React, { ReactElement } from 'react';
import { useMutation, gql } from '@apollo/client';
import { GET_COURSE, Coursedata } from '../CourseTab/graphql/query'
import { SAVE_CHAPTER, Chapter, newChapterdetails } from './graphql/mutation'
import { useQuery } from '@apollo/client';
import { GET_MODULE, Moduledata } from '../ModuleTab/graphql/query';


const AddModuleElement = (): ReactElement => {
  const [coursename, setCourseName] = React.useState('');
  const [modulename, setModuleName] = React.useState('');
  const [chaptername, setChapterName] = React.useState('');
  const [videoId, setvideoId] = React.useState('');
  const [uploadF1, setuploadF1Name] = React.useState('');
  const [uploadF2, setuploadF2Name] = React.useState('');

  const course = useQuery<Coursedata>(
    GET_COURSE
  );
  const module = useQuery<Moduledata>(
    GET_MODULE
  );
  const [createChapter, { error, data }] = useMutation<
    { createChapter: Chapter },
    { chapter: newChapterdetails }
  >(SAVE_CHAPTER, {
    variables: { chapter: { chaptername, modulename, videoId, uploadF1, uploadF2 } }
  });
  function refresh() {
    window.location.reload();
  }
  return <>
    <div className="flex">
      <div className="block p-6 rounded-lg  bg-[#D9D9D9]">
        <select id="countries" value={modulename} onChange={(e) => setModuleName(e.target.value)} className=" text-xs font-poppins  border-none focus:outline-none  rounded  bg-white text-gray-400 block w-full p-2.5 mb-3">
          <option selected>Select Module</option>
          {module.data && module.data.getallmodule.map((course: any, index: any) => {
            return (<option selected>{course.modulename}</option>)
          })}
        </select>
        <div className="mb-3 xl:w-96">
          <input
            type="text"
            className="
                                    form-control
                                    block
                                    w-full
                                    px-3
                                    py-3
                                    text-xs
                                    font-normal
                                    text-gray-700
                                    bg-white bg-clip-padding
                                    border-none
                                    rounded
                                    font-poppins
                                    transition
                                    ease-in-out
                                   
                                    m-0
                                    focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
                                    "
            id="exampleText0"
            placeholder="Enter Chapter  name"
            value={chaptername} onChange={(e) => setChapterName(e.target.value)}
            
          />
        </div>
        <div className="mb-3 xl:w-96">
          <input
            type="text"
            className="
                                    form-control
                                    block
                                    w-full
                                    px-3
                                    py-3
                                    text-xs
                                    font-normal
                                    text-gray-700
                                    bg-white bg-clip-padding
                                    border-none
                                    rounded
                                    font-poppins
                                    transition
                                    ease-in-out
                                   
                                    m-0
                                    focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
                                    "
            id="exampleText0"
            placeholder="Enter video Stream ID"
            value={videoId} onChange={(e) => setvideoId(e.target.value)}
          
          />
        </div>

        <div className="flex items-center justify-center w-full mb-3">
          <label className="flex flex-col items-left  justify-center w-full   rounded cursor-pointer bg-[#F5F2F2] dark:hover:bg-bray-800 dark:bg-gray-700 hover:bg-gray-100 dark:border-gray-600 dark:hover:border-gray-500 dark:hover:bg-gray-600">
            <p className="p-3 font-normal inline text-xs text-gray-400 font-poppins">Upload Instructor Guide</p>

            <input id="dropzone-file" type="file" className="hidden"  value={uploadF1} onChange={(e) => setuploadF1Name(e.target.value)}/>
          </label>
        </div>
        <div className="flex items-center justify-center w-full mb-3">
          <label className="flex flex-col items-left  justify-center w-full   rounded cursor-pointer bg-[#F5F2F2] dark:hover:bg-bray-800 dark:bg-gray-700 hover:bg-gray-100 dark:border-gray-600 dark:hover:border-gray-500 dark:hover:bg-gray-600">
            <p className="p-3 font-normal inline text-xs text-gray-400 font-poppins">Upload Code Material</p>

            <input id="dropzone-file" type="file" className="hidden"  value={uploadF2} onChange={(e) => setuploadF2Name(e.target.value)}/>
          </label>
        </div>
        <select id="countries" className=" text-xs font-poppins  border-none focus:outline-none  rounded  bg-[#F5F2F2] text-gray-400 block w-full p-2.5 mb-3">
          <option selected>Is this Workshop/Last Chapter</option>

        </select>

        <button type="button" onClick={() => {chaptername && createChapter()}} className=" inline-block xl:w-96 px-6 py-3 bg-black text-white font-medium text-sm w-100 leading-tight border-0  text-opacity-70 hover:shadow-sm  rounded  hover:border-0  hover:bg-gray-800   focus:outline-none  active:bg-black-800 active:shadow-lg transition duration-150 ease-in-out">Add Chapter</button>
      </div>
    </div>


  </>;
};
export default AddModuleElement;