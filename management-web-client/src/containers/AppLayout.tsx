import React, { ReactElement } from 'react';

type AppLayoutProps = {
  children: React.ReactNode;
};

const AppLayout = ({ children }: AppLayoutProps): ReactElement => {
  return (
    <div className="bg-white">
      <div>{children}</div>
    </div>
  );
};

export default AppLayout;
