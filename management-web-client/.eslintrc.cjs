module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    'airbnb-base',
    'airbnb-typescript',
    'airbnb/hooks',
    'plugin:prettier/recommended',
    'plugin:@typescript-eslint/recommended',
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
    },
    project: ['./tsconfig.json'],
  },
  plugins: ['react', '@typescript-eslint'],
  rules: {
    "semi": 2,
    "@typescript-eslint/no-explicit-any": 1,
    "@typescript-eslint/explicit-module-boundary-types": 1,
    "@typescript-eslint/no-unused-vars": 2,
    "no-console": 1,
    "import/order": 2,
    "@typescript-eslint/semi": [
      2
    ],
    "comma-dangle": [
      "error",
      "always-multiline"
    ],
    "import/no-extraneous-dependencies": [
      2,
      {
        "devDependencies": [
          "src/setupTests.js",
          "**/*.test.js"
        ]
      }
    ],
    "react/jsx-filename-extension": [
      2,
      {
        "extensions": [
          ".tsx",
          ".ts"
        ]
      }
    ],
    "prettier/prettier": [
      "error",
      {
        "endOfLine": "lf",
        "printWidth": 100,
        "tabWidth": 2,
        "semi": true,
        "singleQuote": true,
        "trailingComma": "all",
        "bracketSpacing": true,
        "jsxBracketSameLine": false,
        "arrowParens": "always",
        "proseWrap": "always"
      }
    ],
    "no-shadow": "off",
    "@typescript-eslint/no-shadow": [
      "warn"
    ]
  }
};
